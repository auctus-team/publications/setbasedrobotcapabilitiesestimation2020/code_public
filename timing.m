close all;
clear;
clc;

format long
nruns = 20;

%% Add paths
addpath("plotting");
addpath("src");

%% Interval linear system    
n = 7;
m = 6;

A_mid = [-0.440381132420361   0.317547092079286   0.300330575352401  -0.109062197676264   0.156859890973707  -0.393783655071336
       0.181971904149063   0.222439592366842  -0.046202291273081   0.331379742839070   0.127973359190104  -0.127590259944463
      -0.457568862499258  -0.350134557522033  -0.067608496216538   0.303364391602440  -0.208015920038285  -0.301881597457025
      -0.428554535399358   0.159605252908307   0.325313795402046  -0.439528820830106  -0.068348829751280  -0.010312361983976
       0.021649842464284   0.018594942510538  -0.416530185141086  -0.100742229386424  -0.484512874363981  -0.160506586609242
      -0.403269974219133   0.472974554763863  -0.366828992392838   0.026875830508296   0.484063724379154   0.451630464777727
       0.318148553859625   0.148991492712356  -0.326611386880994  -0.083200532069213  -0.332831590085344   0.420332039836564];
% A_rad = 0.01*ones(n,m);
A_rad = 0.0*ones(n,m);
A_min = A_mid - A_rad;
A_max = A_mid + A_rad;

b_mid =[0.117666389588455
        0.359442305646212
        0.305489424529686
        0.076721515614685
        -0.317077530585086
        -0.260067989431283
        0.386511933076101];
b_rad = ones(n,1);
b_min = b_mid - b_rad;
b_max = b_mid + b_rad;

x_mid = [-0.471325847535894
        -0.010098611487776
        -0.332072854317743
        0.478680649641159
        0.212694471678914
        0.000471624154843
        -0.028911625458061];
x_rad = ones(n,1);
x_min = x_mid - x_rad;
x_max = x_mid + x_rad;

xc = zeros(m,1);
bc = zeros(m,1);

%% Test functions
[~, r1] = Sigma_AE_Ab_nball(A_min,A_max,b_min,b_max,xc);
[~, r2] = Sigma_AE_Ab_nball(A_min,A_max,b_min,b_max,[]);
[~, r3] = Sigma_AE_Ab_ncube(A_min,A_max,b_min,b_max,xc);
[~, r4] = Sigma_AE_Ab_ncube(A_min,A_max,b_min,b_max,[]);
[vertices1] = Sigma_AE_Ab_polytope(A_min,A_max,b_min,b_max);
if r1==0 || r2==0 || r3==0 || r4==0 || isempty(vertices1)
    error("System cannot be solved for Sigma_AE_Ab")
end
[~, r5, ~] = Omega_AE_Ax_nball(A_min',A_max',b_min,b_max,xc);
[~, r6, ~] = Omega_AE_Ax_nball(A_min',A_max',b_min,b_max,[]);
[~, r7, ~] = Omega_AE_Ax_ncube(A_min',A_max',b_min,b_max,xc);
[~, r8, ~] = Omega_AE_Ax_ncube(A_min',A_max',b_min,b_max,[]);
[vertices2, ~] = Omega_AE_Ax_polytope(A_min',A_max',b_min,b_max);
if r5==0 || r6==0 || r7==0 || r8==0 || isempty(vertices2)
    error("System cannot be solved for Omega_AE_Ax")
end

%% Timing Sigma_AE_Ab (Ax=b)
f = @() Sigma_AE_Ab_nball(A_min,A_max,b_min,b_max,xc); % handle to function
times = [];
for t=1:nruns
    times(t) = timeit(f)*1000;
end
Sigma_AE_Ab_nball_meantime = mean(times)
std(times)

f = @() Sigma_AE_Ab_nball(A_min,A_max,b_min,b_max,[]); % handle to function
times = [];
for i=1:nruns
    times(i) = timeit(f)*1000;
end
Sigma_AE_Ab_nball_var_meantime = mean(times)
std(times)

f = @() Sigma_AE_Ab_ncube(A_min,A_max,b_min,b_max,xc); % handle to function
times = [];
for t=1:nruns
    times(t) = timeit(f)*1000;
end
Sigma_AE_Ab_ncube_meantime = mean(times)
std(times)

f = @() Sigma_AE_Ab_ncube(A_min,A_max,b_min,b_max,[]); % handle to function
times = [];
for i=1:nruns
    times(i) = timeit(f)*1000;
end
Sigma_AE_Ab_ncube_var_meantime = mean(times)
std(times)

f = @() Sigma_AE_Ab_polytope(A_min,A_max,b_min,b_max); % handle to function
times = [];
for i=1:nruns
    times(i) = timeit(f)*1000;
end
Sigma_AE_Ab_polytope_meantime = mean(times)
std(times)

%% Timing Omega_AE_Ax (A'x=b)

f = @() Omega_AE_Ax_nball(A_min',A_max',x_min,x_max,bc); % handle to function
times = [];
for t=1:nruns
    times(t) = timeit(f)*1000;
end
Omega_AE_Ax_nball_meantime = mean(times)
std(times)

f = @() Omega_AE_Ax_nball(A_min',A_max',x_min,x_max,[]); % handle to function
times = [];
for i=1:nruns
    times(i) = timeit(f)*1000;
end
Omega_AE_Ax_nball_var_meantime = mean(times)
std(times)

f = @() Omega_AE_Ax_ncube(A_min',A_max',x_min,x_max,bc); % handle to function
times = [];
for t=1:nruns
    times(t) = timeit(f)*1000;
end
Omega_AE_Ax_ncube_meantime = mean(times)
std(times)

f = @() Omega_AE_Ax_ncube(A_min',A_max',x_min,x_max,[]); % handle to function
times = [];
for i=1:nruns
    times(i) = timeit(f)*1000;
end
Omega_AE_Ax_ncube_var_meantime = mean(times)
std(times)

f = @() Omega_AE_Ax_polytope(A_min',A_max',x_min,x_max); % handle to function
times = [];
for i=1:nruns
    times(i) = timeit(f)*1000;
end
Omega_AE_Ax_polytope_meantime = mean(times)
std(times)
