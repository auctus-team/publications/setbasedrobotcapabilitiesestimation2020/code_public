function [x,r] = nball_variable_center(A_min,A_max,b_min,b_max)
    [m,n] = size(A_min);

    % x = [x1...xn, r]
    f = [zeros(1,n),-1];

    A = [];
    b = [];

    combs = dec2bin(0:(2^n-1),n) - '0';
    for i=1:m
        for c=1:size(combs,1)
            ai = nan(1,n);
            for j=1:n
                if (combs(c,j)==1)
                    ai(j) = A_max(i,j);
                else
                    ai(j) = A_min(i,j);
                end
            end
            sup_ni = norm(ai,2);
            A = [A; ai, sup_ni];
            b = [b; b_max(i)];
        end
    end
    for i=1:m
        for c=1:size(combs,1)
            ai = nan(1,n);
            for j=1:n
                if (combs(c,j)==1)
                    ai(j) = A_max(i,j);
                else
                    ai(j) = A_min(i,j);
                end
            end
            sup_ni = norm(ai,2);
            A = [A; -ai, sup_ni];
            b = [b; -b_min(i)];
        end
    end

    Aeq = [];
    beq = [];

    LB = -Inf * ones(length(f),1);
    LB(end) = 0;
    UB = Inf * ones(length(f),1);

    options = optimset('linprog');
    options.Display = 'off';
    xlinprog = linprog(f,A,b,Aeq,beq,LB,UB,options);
    x = xlinprog(1:n);
    r = xlinprog(end);
end
