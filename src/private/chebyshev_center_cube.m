function [x,r] = chebyshev_center_cube(H,d)
    [m,n] = size(H);

    % x = [x1...xn, r]
    f = [zeros(1,n),-1];

    A = [];
    b = [];
    for i=1:m    
        hi_1norm = norm((H(i,:)),1);    
        row = [H(i,1:n),hi_1norm];
        A = [A; row];
        b = [b; d(i)];
    end


    Aeq = [];
    beq = [];

    LB = -Inf * ones(length(f),1);
    LB(end) = 0;
    UB = Inf * ones(length(f),1);

    options = optimset('linprog');
    options.Display = 'off';
    xlinprog = linprog(f,A,b,Aeq,beq,LB,UB,options);
    x = xlinprog(1:n);
    r = xlinprog(end);
end