function [x,r] = ncube_variable_center(A_min,A_max,b_min,b_max)
    [m,n] = size(A_min);

    % x = [x1...xn, y11..y1n, ..., ym1...ymn, z11..z1n, ..., zm1...zmn, r]
    f = [zeros(1,n),zeros(1,2*m*n),-1];

    A = [];
    b = [];
    for i=1:m
        % eq33b
        A = [A; zeros(1,n),zeros(1,(i-1)*n),ones(1,n),zeros(1,(m-i)*n),zeros(1,m*n),zeros(1,1)];
        b = [b; b_max(i)];
        for j=1:n
            % eq33c
            eq33c = [zeros(1,n),zeros(1,(i-1)*n+(j-1)),-1,zeros(1,(n-j)),zeros(1,(m-i)*n),zeros(1,m*n),A_max(i,j)];
            eq33c(j) = A_max(i,j);

            % eq33d
            eq33d = [zeros(1,n),zeros(1,(i-1)*n+(j-1)),-1,zeros(1,(n-j)),zeros(1,(m-i)*n),zeros(1,m*n),-A_max(i,j)];
            eq33d(j) = A_max(i,j);

            % eq33e
            eq33e = [zeros(1,n),zeros(1,(i-1)*n+(j-1)),-1,zeros(1,(n-j)),zeros(1,(m-i)*n),zeros(1,m*n),A_min(i,j)];
            eq33e(j) = A_min(i,j);

            % eq33f
            eq33f = [zeros(1,n),zeros(1,(i-1)*n+(j-1)),-1,zeros(1,(n-j)),zeros(1,(m-i)*n),zeros(1,m*n),-A_min(i,j)];
            eq33f(j) = A_min(i,j);
            A = [A; eq33c; eq33d; eq33e; eq33f];
            b = [b; 0;0;0;0];
        end
    end
    for i=1:m
        % eq33g
        A = [A; zeros(1,n),zeros(1,m*n),zeros(1,(i-1)*n),-ones(1,n),zeros(1,(m-i)*n),zeros(1,1)];
        b = [b; -b_min(i)];
        for j=1:n
            % eq33h
            eq33h = [zeros(1,n),zeros(1,m*n),zeros(1,(i-1)*n+(j-1)),1,zeros(1,(n-j)),zeros(1,(m-i)*n),-A_max(i,j)];
            eq33h(j) = -A_max(i,j);

            % eq33i
            eq33i = [zeros(1,n),zeros(1,m*n),zeros(1,(i-1)*n+(j-1)),1,zeros(1,(n-j)),zeros(1,(m-i)*n),A_max(i,j)];
            eq33i(j) = -A_max(i,j);

            % eq33j
            eq33j = [zeros(1,n),zeros(1,m*n),zeros(1,(i-1)*n+(j-1)),1,zeros(1,(n-j)),zeros(1,(m-i)*n),-A_min(i,j)];
            eq33j(j) = -A_min(i,j);

            % eq33k
            eq33k = [zeros(1,n),zeros(1,m*n),zeros(1,(i-1)*n+(j-1)),1,zeros(1,(n-j)),zeros(1,(m-i)*n),A_min(i,j)];
            eq33k(j) = -A_min(i,j);
            A = [A; eq33h; eq33i; eq33j; eq33k];
            b = [b; 0;0;0;0];
        end
    end

    Aeq = [];
    beq = [];

    LB = -Inf * ones(length(f),1);
    LB(end) = 0;
    UB = Inf * ones(length(f),1);

    options = optimset('linprog');
    options.Display = 'off';
    xlinprog = linprog(f,A,b,Aeq,beq,LB,UB,options);
    x = xlinprog(1:n);
    r = xlinprog(end);
end