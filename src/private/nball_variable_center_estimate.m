function [x,r] = nball_variable_center_estimate(A_min,A_max,b_min,b_max)
  [m,n] = size(A_min);

  % x = [x1...xn, y1...yn, r]
  f = [zeros(1,n),zeros(1,n),-1];

  A_mid = (A_min + A_max) / 2;
  A_rad = (A_max - A_min) / 2;

  A = [];
  b = [];

  for i=1:m
      si = norm(sqrt(max(A_min(i,:).^2,A_max(i,:).^2)),2);
      
      A = [A; 
          A_mid(i,:),A_rad(i,:),si;
          -A_mid(i,:),A_rad(i,:),si];
      b = [b; 
          b_max(i);
          -b_min(i)];
  end

  for j=1:n       
       A = [A; 
           [zeros(1,(j-1)),1,zeros(1,(n-j)),zeros(1,(j-1)),-1,zeros(1,(n-j)),0];
           [zeros(1,(j-1)),-1,zeros(1,(n-j)),zeros(1,(j-1)),-1,zeros(1,(n-j)),0]];
       b = [b; 
           0;
           0];
  end

  Aeq = [];
  beq = [];

  LB = -Inf * ones(length(f),1);
  LB(end) = 0;
  UB = Inf * ones(length(f),1);

  options = optimset('linprog');
  options.Display = 'off';
  xlinprog = linprog(f,A,b,Aeq,beq,LB,UB,options);
  x = xlinprog(1:n);
  r = xlinprog(end);
end
