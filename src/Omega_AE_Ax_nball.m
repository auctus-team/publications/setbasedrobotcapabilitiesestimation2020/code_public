function [bc, r_ball, r_polytope] = Omega_AE_Ax_nball(A_min,A_max,x_min,x_max,bc)  
% Omega_AE_Ax_nball For a given interval linear system of equations 
% Ax = b, for all A in [A_min,A_max], exists x in [x_min,x_max], computes 
% an inner approximation of the largest inscribed n-ball 
% B = {b | ||b-bc|| <= r} in the corresponding set of b. 
% When A_min < A_max the inscribed polytope is approximated by
% mid([A])y = b, where y = mid([x]) + [-rp,rp]rad([x]).
%
%   [bc, r, rp] = Omega_AE_Ax_nball(A_min,A_max,x_min,x_max,bc) returns
%   the largest n-ball r value centered at the given bc with polytope
%   approximation rp.
%
%   [bc, r, rp] = Omega_AE_Ax_nball(A_min,A_max,x_min,x_max,[]) returns
%   the largest n-ball r value and corresponding center bc with polytope
%   approximation rp.

    assert(size(A_min,1) <= size(A_min,2));  
    assert(size(A_min,1) == size(A_max,1));   
    assert(size(A_min,2) == size(A_max,2)); 
    assert(size(x_min,1) == size(x_max,1));  
    assert(size(A_min,2) == size(x_min,1));   
    assert(isempty(bc) || size(A_min,1) == size(bc,1)); 
    assert(all(A_max >= A_min, 'all'));  
    assert(all(x_max >= x_min, 'all'));
    
    A_mid = (A_max + A_min) / 2;
    A_rad = (A_max - A_min) / 2;
    x_rad = (x_max - x_min) / 2;
    x_mid = (x_max + x_min) / 2;
    x_mag = max(abs(x_min - x_mid),abs(x_max - x_mid));

    %% Check if A is an interval matrix
    if A_min == A_max
        is_interval = false;
    else
        is_interval = true;
    end    

    %% Polytope r value    
    r_polytope = 1;
    if is_interval
        % Compute r_polytope
        if rank(A_mid)==size(A_mid,1)
            r_polytope = max(0,min((x_rad - abs((pinv(A_mid))) * A_rad * x_mag) ./ x_rad));    
        else
            r_polytope = 0;
        end
        
        % Update x
        x_min = x_mid - r_polytope*x_rad;
        x_max = x_mid + r_polytope*x_rad;
        
        if r_polytope == 0
            r_ball = 0;
            return;
        end
    end
    
    %% Inscribed n-ball
    [H, d] = hyperplane_shifting_method(A_mid,x_min,x_max);
    if ~isempty(bc)
        %% Use r of inscribed n-ball centered at xc
        if ~isempty(H)
            % Update sphere radius using Chebyshev center formula        
            r_ball = max(0, min((d - H * bc) ./ vecnorm(H,2,2)));
        else
            r_ball = 0;
        end        
    else
        %% Compute r of inscribed n-ball with variable xc
        [bc, r_ball] = chebyshev_center_sphere(H,d);
    end
end
