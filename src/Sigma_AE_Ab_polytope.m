function [polytope_vertices] = Sigma_AE_Ab_polytope(A_min,A_max,b_min,b_max) 
% Sigma_AE_Ab_ncube For a given interval linear system of equations 
% Ax = b, for all A in [A_min,A_max], exists b in [b_min,b_max], computes 
% an inner approximation of the largest inscribed polytope in the corresponding 
% set of x.
%
%   [polytope_vertices] = Sigma_AE_Ab_polytope(A_min,A_max,b_min,b_max) returns
%   the set of polytope vertices.

    assert(size(A_min,1) >= size(A_min,2));  
    assert(size(A_min,1) == size(A_max,1));   
    assert(size(A_min,2) == size(A_max,2)); 
    assert(size(b_min,1) == size(b_max,1)); 
    assert(size(b_max,2) == size(b_max,2)); 
    assert(size(A_min,1) == size(b_min,1)); 
    assert(all(A_max >= A_min, 'all'));  
    assert(all(b_max >= b_min, 'all'));
    
    A_mid = (A_max + A_min) / 2;
        
    %% Check if A is an interval matrix
    if A_min == A_max
        is_interval = false;
    else
        is_interval = true;
    end

    %% Compute polytope
    polytope_vertices = [];
    if is_interval
        Ain = [A_max , -A_min;
               -A_min , A_max;
               -eye(2*size(A_mid,2),2*size(A_mid,2))
        ];
        bin = [b_max;
               -b_min;
               zeros(2*size(A_mid,2),1)
        ];
        dim = size(Ain,2)/2;
        vert = con2vert(Ain, bin, 0.1*ones(dim*2,1)); 
        polytope_vertices = vert(:,1:dim) - vert(:,dim+1:end);
    else
        m = size(A_mid,2);
        n = size(A_mid,1);   
        if n>=m
            % Compute SVD of A_mid
            [U,~,~] = svd(A_mid);

            % Get column space from A (first r columns)
            r = rank(A_mid);
            U1 = U(:,1:r);

            % Create matrix A__ and vector b_
            A_ = zeros(2*n, m);
            b_  = zeros(2*n, 1);
            for i=1:m
                for j=1:n
                    if b_max(j) >= 0
                        A_(j,i) = U1(j,i);
                    else
                        A_(j,i) = -U1(j,i);
                    end
                    b_(j) = abs(b_max(j));

                    if b_min(j) >= 0
                        A_(j+n,i) = U1(j,i);
                    else
                        A_(j+n,i) = -U1(j,i);
                    end
                    b_(j+n) = abs(b_min(j));
                end
            end
            A__ = [A_, eye(2*n, 2*n)];

            % Remove all combinations of m columns from A__ to get (2n x 2n) matrix A_reduced
            col_combs = nchoosek(m+1:m+2*n, 2*n-m);            
            for comb=1:size(col_combs,1)
                col_comb = col_combs(comb,:);

                % Get A_reduced
                A_reduced = A__(:,[1:m,col_comb]);

                % Check rank of A_reduced
                full_rank = rank(A_reduced)==2*n;

                if full_rank
                    % Solve for x_reduced
                    x_reduced = inv(A_reduced) * b_;

                    % Check if m+1:2n are positive
                    positive_slack_vars = all(x_reduced(m+1:2*n)>=0);

                    if positive_slack_vars            
                        % Compute torque vertex
                        b_vertex = U1 * x_reduced(1:m);                        
%                         b_vertices = [b_vertices,b_vertex];

                        try 
                            A_inv = inv(A_mid);
                        catch
                            A_inv = inv(A_mid' * A_mid) * A_mid';
                        end

                        % Compute force vertex
                        x_vertex = A_inv * b_vertex;
                        polytope_vertices = [polytope_vertices;x_vertex'];
                    end
                end
            end
        end
    end
end