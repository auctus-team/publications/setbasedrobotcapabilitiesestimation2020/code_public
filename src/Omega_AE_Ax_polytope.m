function [polytope_vertices, r_polytope] = Omega_AE_Ax_polytope(A_min,A_max,x_min,x_max)  
% Omega_AE_Ax_polytope For a given interval linear system of equations 
% Ax = b, for all A in [A_min,A_max], exists x in [x_min,x_max], computes 
% an inner approximation of the largest inscribed polytope in the corresponding 
% set of b. When A_min < A_max the inscribed polytope is approximated by
% mid([A])y = b, where y = mid([x]) + [-r,r]rad([x]).
%
%   [vertices, r] = Omega_AE_Ax_polytope(A_min,A_max,x_min,x_max)
%   returns the set of polytope vertices and corresponding r value.

    assert(size(A_min,1) <= size(A_min,2));  
    assert(size(A_min,1) == size(A_max,1));   
    assert(size(A_min,2) == size(A_max,2)); 
    assert(size(x_min,1) == size(x_max,1));  
    assert(size(A_min,2) == size(x_min,1));   
    assert(all(A_max >= A_min, 'all'));  
    assert(all(x_max >= x_min, 'all'));
    
    A_mid = (A_max + A_min) / 2;
    A_rad = (A_max - A_min) / 2;
    x_rad = (x_max - x_min) / 2;
    x_mid = (x_max + x_min) / 2;
    x_mag = max(abs(x_min - x_mid),abs(x_max - x_mid));

    %% Check if A is an interval matrix
    if A_min == A_max
        is_interval = false;
    else
        is_interval = true;
    end    

    %% Polytope r value    
    r_polytope = 1;
    if is_interval
        % Compute r_polytope
        if rank(A_mid)==size(A_mid,1)
            r_polytope = max(0,min((x_rad - abs((pinv(A_mid))) * A_rad * x_mag) ./ x_rad));    
        else
            r_polytope = 0;
        end
        
        % Update x
        x_min = x_mid - r_polytope*x_rad;
        x_max = x_mid + r_polytope*x_rad;
        
        if r_polytope == 0
            polytope_vertices = [];
            return;
        end
    end
    
    % Polytope vertices
    box_x = create_box(size(x_mid,1),x_min,x_max);
    polytope_vertices = (A_mid * box_x')';
end
