Matlab programs implementing the inner approximation algorithms for interval linear systems of equations proposed in:

*J. K. Pickard, V. Padois, M. Hladik, and D. Daney. "Efficient Set-Based Approaches for the Reliable Computation of Robot Capabilities".*

 The following inner approximations are supported:
1) largest inscribed polytope;
2) largest inscribed n-ball centered at a point;
3) largest inscribed n-ball with variable center;
4) largest inscribed n-cube centered at a point;
5) largest inscribed n-cube with variable center.

# Dependencies
The inner approximations 3 and 5 require the matlab optimization toolbox for `linprog`.

# Running the code
The `src` directory contains the inner approximation algorithms and the `plotting` directory contains programs used for generating plots. 

An example program `example.m` demonstrates how to use the inner approximation algorithms.

