function plot_inner_approx(polytope_vertices, width_cube, sphere_radius, pc, edge, face, linestyle)
    hold on;
     if ~exist('linestyle','var')
        linestyle = '-';
    end
    
    % Polytope
    if ~isempty(polytope_vertices) > 0
        if (size(polytope_vertices,2)==3)
            [k,~] = convhulln(polytope_vertices);
            trisurf(k,polytope_vertices(:,1),polytope_vertices(:,2),polytope_vertices(:,3),'Edgecolor',edge,'FaceColor',face,'LineStyle',linestyle,'FaceAlpha',0.3);
        elseif (size(polytope_vertices,2)==2)
            k = boundary(polytope_vertices(:,1),polytope_vertices(:,2),0);
            fill(polytope_vertices(k,1), polytope_vertices(k,2),edge,'FaceColor',face,'LineStyle',linestyle,'FaceAlpha',0.3);
        end
    end

    % Sphere   
    if sphere_radius > 0
        if (size(pc,1)==3)
            [X,Y,Z] = sphere;
            X = X*sphere_radius + pc(1);
            Y = Y*sphere_radius + pc(2);
            Z = Z*sphere_radius + pc(3);
            surf(X,Y,Z,'Edgecolor',edge,'Facecolor',face,'LineStyle',linestyle,'FaceAlpha',0.3);
        elseif (size(pc,1)==2)
            th = 0:pi/500:2*pi;
            x = sphere_radius * cos(th) + pc(1);
            y = sphere_radius * sin(th) + pc(2);
            fill(x,y,edge,'FaceColor',face,'LineStyle',linestyle,'FaceAlpha',0.3);  
        end
    end

    % Cube
    if width_cube > 0
        cube_min = pc-width_cube;
        cube_max = pc+width_cube;
        S = create_box(size(cube_min,1),cube_min,cube_max);  
        if (size(pc,1)==3)
            [k,~] = convhulln(S);
            trisurf(k,S(:,1),S(:,2),S(:,3),'Edgecolor',edge,'FaceColor',face,'LineStyle',linestyle,'FaceAlpha',0.6);
        elseif (size(pc,1)==2)
            k = boundary(S(:,1),S(:,2),0);
            fill(S(k,1), S(k,2),edge,'FaceColor',face,'LineStyle',linestyle,'FaceAlpha',0.6);
        end
    end
    
    axis equal;